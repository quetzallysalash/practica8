package mx.ticketit.practica8.model

/* Nephtali Gómez Arcive
Parte 2 - Agregar clase Contact con atributos name, age y correo
*/
data class Contact (var name : String, var age: Int, var correo : String)