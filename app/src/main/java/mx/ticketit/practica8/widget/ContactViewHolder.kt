package mx.ticketit.practica8.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.ticketit.practica8.R
import mx.ticketit.practica8.model.Contact


// Parte 3 - ContactViewHolder, utiliza item_contact (layout)

class ContactViewHolder (inflater: LayoutInflater, parent: ViewGroup)  :
    RecyclerView.ViewHolder(inflater.inflate((R.layout.item_contact),parent,false)){

    var txtName : TextView
    var txtAge : TextView
    var txtMail : TextView

    init {
        txtName = itemView.findViewById(R.id.txtName)
        txtAge = itemView.findViewById(R.id.txtAge)
        txtMail = itemView.findViewById(R.id.txtMail)
    }

    // Parte 3 - implementacion de método bind

    fun bind (contact : Contact ) {

        txtName.text = contact.name
        txtAge.text = contact.age.toString()
        txtMail.text = contact.correo

    }



}