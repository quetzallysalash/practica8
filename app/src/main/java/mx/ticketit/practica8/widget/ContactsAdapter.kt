package mx.ticketit.practica8.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mx.ticketit.practica8.model.Contact

// Nephtali Gómez Arcive
// Parte 4 - Creación de clase ContactsAdapter

// Parte 4 - Parámetro de lista de tipo Contact

// Parte 3 - Practica 8 - Atributo para MutableList

class ContactsAdapter(private var list : MutableList<Contact>) : RecyclerView.Adapter<ContactViewHolder>() {

    // Creación de métodos
    // Se ajusta en parte 5 errores en sobreescritura y adapter (commit)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ContactViewHolder {
         val inflater = LayoutInflater.from(parent.context)
         return ContactViewHolder(inflater,parent)
    }

     override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
         val contact : Contact = list[position]
         holder.bind(contact)
    }


     override fun getItemCount():Int {
        return list.size
    }

    // Parte 3 - Practica 8 - Agregado de valores en la lista

    fun add(contacto: Contact) {
        list.add(contacto)
        notifyItemInserted(list.size -1)
    }

}
