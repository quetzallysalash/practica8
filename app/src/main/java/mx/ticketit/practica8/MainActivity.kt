package mx.ticketit.practica8

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import mx.ticketit.practica8.model.Contact
import mx.ticketit.practica8.ui.ContactDialogFragment
import mx.ticketit.practica8.ui.NoticeDialogListener
import mx.ticketit.practica8.widget.ContactsAdapter

class MainActivity : AppCompatActivity(), NoticeDialogListener {

    /*
    Nephtali Gómez Arcive
    Part 5 - Creación de lista tipo contact con valores
     */


    // Parte 3 - Practica 8 - Cambiar a mutable list

    private var Contacts = mutableListOf(
        Contact("NGA", 26, "nga@ticketit.mx")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        /*
   Nephtali Gómez Arcive
   Part 5 - Android extensions - implementación de list_recycler_view
    */

        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ContactsAdapter(Contacts)

            // Parte 4 - Agregado de separadores
            addItemDecoration(DividerItemDecoration(context,LinearLayoutManager.VERTICAL))
        }


        /*
        Nephtali Gómez Arcive
        Part 1 - PRACTICA 8 - Implementar llamado en el evento OnClickListener para mostrar Dialog
        */
        fab.setOnClickListener { view ->

            val newFragment =   ContactDialogFragment()
            newFragment.show(supportFragmentManager,"Contacto")

            /*
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
            */

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, contact: Contact) {
        // Parte 3 - Practica 8 - Referencia del adapter

        val adapter = list_recycler_view.adapter as ContactsAdapter
        adapter.add(contact)

        // Parte 2 - Practica 8 - Agregar Snackbar

        Snackbar.make(findViewById(android.R.id.content), "${contact.name} agregado", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()

    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {

        Snackbar.make(findViewById(android.R.id.content), "Cancelado", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
    }

}
