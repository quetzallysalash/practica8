package mx.ticketit.practica8.ui

import androidx.fragment.app.DialogFragment
import mx.ticketit.practica8.model.Contact

interface NoticeDialogListener {

    /*
    Práctica 8 - Parte 2 - Creación de interface para DialogFragment
    * */

    fun onDialogPositiveClick(dialog : DialogFragment, contact: Contact)
    fun onDialogNegativeClick(dialog: DialogFragment)

}