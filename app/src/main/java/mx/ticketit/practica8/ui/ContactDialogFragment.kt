package mx.ticketit.practica8.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_contact.view.*
import mx.ticketit.practica8.R
import mx.ticketit.practica8.model.Contact
import java.lang.ClassCastException
import java.lang.IllegalStateException

/*
Autor: Nephtali Gómez Arcive
Parte 1 - Creación de clase ContactDialogFragment con herencia de DialogFragment

 */

class ContactDialogFragment : DialogFragment () {

    /*
Práctica 8 - Parte 2 - Implementación de NoticeDialogListener
* */

    internal lateinit var listener : NoticeDialogListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            val builder = AlertDialog.Builder(it)

            val inflater = requireActivity().layoutInflater

            val view = inflater.inflate(R.layout.dialog_contact,null)

            builder.setTitle("Nuevo contacto")
            builder.setView(view)
                .setPositiveButton("Agregar",
                    DialogInterface.OnClickListener { dialogInterface, i ->

                        var contacto = Contact(

                            view.txtName.editableText.toString(),
                            view.txtAge.editableText.toString().toInt(),
                            view.txtMail.editableText.toString()
                        )

                        listener.onDialogPositiveClick( this,contacto)

                    })

                .setNegativeButton("Cancelar",
                    DialogInterface.OnClickListener { dialogInterface, i ->

                        listener.onDialogNegativeClick( this)

                    })

            builder.create()

        } ?: throw IllegalStateException("La actividad NO PUEDE estar vaciía")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            listener = context as NoticeDialogListener
        } catch (e : ClassCastException) {
            throw ClassCastException("Error en implementación")

        }
    }


}